﻿using Microsoft.Toolkit.Uwp.Notifications;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace TestNotification
{
    internal class Program
    {
        public static void SetNotificationText(string text)
        {
            new ToastContentBuilder().AddText("Test").AddText(text).Show();
        }
        static void Main(string[] args)
        {
            Console.WriteLine("Testing...");
            SetNotificationText("Hello World");
            Thread.Sleep(5000);
            Console.WriteLine("Test Is Complete Successfully!!!");
            Thread.Sleep(8000);
            Environment.Exit(332);
        }
    }
}
